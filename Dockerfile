FROM nginx
MAINTAINER Leonardo Jurado <ljuradog@gmail.com>
RUN apt-get update && apt-get upgrade
RUN apt-get install -y vim nginx
RUN apt-get install -y php5 php5-fpm php5-pgsql

RUN groupadd leonardojurado
RUN useradd -g leonardojurado leonardojurado

#ADD leonardojurado.conf /etc/php5/fpm/pool.d/
ADD default.conf /etc/nginx/conf.d/
ADD index.php /var/www/html/leonardojurado.com/public_html/
RUN chown -R www-data:www-data /var/www/html/leonardojurado.com/
RUN chmod -R 755 /var/www
EXPOSE 80

ENTRYPOINT /usr/sbin/php5-fpm && nginx -g 'daemon off;'

